## [1.18.1](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.18.0...v1.18.1) (2024-07-21)


### Bug Fixes

* docker network creation ([b06927a](https://gitlab.com/hove-ict/tools/ci/templates/commit/b06927ab16a71775bf21348ba67b340a1ee58f46))

# [1.18.0](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.17.16...v1.18.0) (2024-06-08)


### Features

* single docker job ([5247e58](https://gitlab.com/hove-ict/tools/ci/templates/commit/5247e58180139794a07f8faa5421f4626af17a91))

## [1.17.16](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.17.15...v1.17.16) (2024-05-28)


### Bug Fixes

* allow custom compose files ([972eccb](https://gitlab.com/hove-ict/tools/ci/templates/commit/972eccbd344de3467d2e0b3046669600d6ffcc15))

## [1.17.15](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.17.14...v1.17.15) (2024-05-08)


### Bug Fixes

* docker login stdin + referenced ([f9dfcfc](https://gitlab.com/hove-ict/tools/ci/templates/commit/f9dfcfce5f33acabe1fd32b199aa44b4c84c857a))

## [1.17.14](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.17.13...v1.17.14) (2024-05-07)


### Bug Fixes

* use docker compose config ([f8f6961](https://gitlab.com/hove-ict/tools/ci/templates/commit/f8f69615619b5f48308d704e77187a907957e1a9))

## [1.17.13](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.17.12...v1.17.13) (2024-05-07)


### Bug Fixes

* allow specific network creation job skip ([32cded8](https://gitlab.com/hove-ict/tools/ci/templates/commit/32cded8960ae9d4e0b8beac34b2415ae325f3731))

## [1.17.12](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.17.11...v1.17.12) (2024-05-07)


### Bug Fixes

* optional needs syntax ([9589aec](https://gitlab.com/hove-ict/tools/ci/templates/commit/9589aecb07dea34389a3d291f6a755367b2eedba))

## [1.17.11](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.17.10...v1.17.11) (2024-05-07)


### Bug Fixes

* check if need to wait for compose file download ([148daeb](https://gitlab.com/hove-ict/tools/ci/templates/commit/148daeb20860da9a33b8cd35df62fc1f30f9d8d7))

## [1.17.10](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.17.9...v1.17.10) (2024-05-07)


### Bug Fixes

* expire artifacts ([b5def8b](https://gitlab.com/hove-ict/tools/ci/templates/commit/b5def8b215facb41f350f741ebc35c539daf3241))

## [1.17.9](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.17.8...v1.17.9) (2024-05-05)


### Bug Fixes

* add stack deploy args; default dont detach ([799ad59](https://gitlab.com/hove-ict/tools/ci/templates/commit/799ad590df7e58632f0a227e847a60c766bca21e))

## [1.17.8](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.17.7...v1.17.8) (2024-03-11)


### Bug Fixes

* add rule to skip network ([670a984](https://gitlab.com/hove-ict/tools/ci/templates/commit/670a98440a0b47abdcc63b9a9f667cb69756af2f))

## [1.17.7](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.17.6...v1.17.7) (2024-02-20)


### Bug Fixes

* pull before stack deploy ([6c329de](https://gitlab.com/hove-ict/tools/ci/templates/commit/6c329de1f46f86b14c83e803a8a53eb445ce8247))

## [1.17.6](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.17.5...v1.17.6) (2024-01-13)


### Bug Fixes

* pinned update check ([b506254](https://gitlab.com/hove-ict/tools/ci/templates/commit/b506254ef55548a6321053f2d9248163d20cf427))

## [1.17.5](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.17.4...v1.17.5) (2024-01-13)


### Bug Fixes

* update repo ([2b54377](https://gitlab.com/hove-ict/tools/ci/templates/commit/2b54377cc087e35943b31a301cfa649a5aa1c69f))

## [1.17.4](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.17.3...v1.17.4) (2024-01-13)


### Bug Fixes

* update check ([c8445ce](https://gitlab.com/hove-ict/tools/ci/templates/commit/c8445ce652e4684f470fe6bbff0926206c9c5f41))

## [1.17.3](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.17.2...v1.17.3) (2023-12-23)


### Bug Fixes

* stack config version ([619c2a1](https://gitlab.com/hove-ict/tools/ci/templates/commit/619c2a127994df51dfd06f0245e13d26b3ac82a1))

## [1.17.2](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.17.1...v1.17.2) (2023-12-21)


### Bug Fixes

* get network from name field value ([f661d6d](https://gitlab.com/hove-ict/tools/ci/templates/commit/f661d6d816c8be4259f5000af318ae6726737a63))

## [1.17.1](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.17.0...v1.17.1) (2023-12-21)


### Bug Fixes

* check for existing compose config ([6d69089](https://gitlab.com/hove-ict/tools/ci/templates/commit/6d69089415835c3be0e415c141bcf4279b3519e6))

# [1.17.0](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.16.0...v1.17.0) (2023-10-26)


### Features

* squash history ([a1e62af](https://gitlab.com/hove-ict/tools/ci/templates/commit/a1e62af6dd59918b442588a05026d5cddd329d1b))

# [1.16.0](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.15.0...v1.16.0) (2023-10-26)


### Features

* allow commit as LFS ([9927747](https://gitlab.com/hove-ict/tools/ci/templates/commit/99277473b17d2dd8219f2a7db8d45a9e5e536391))

# [1.15.0](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.14.2...v1.15.0) (2023-10-26)


### Features

* allow data skip ([d00592d](https://gitlab.com/hove-ict/tools/ci/templates/commit/d00592dfc23fea91918c63cd6f2f1a404cfaf8a7))

## [1.14.2](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.14.1...v1.14.2) (2023-07-11)


### Bug Fixes

* test deploy key ([4a64ed0](https://gitlab.com/hove-ict/tools/ci/templates/commit/4a64ed0dcd94e0ab743c1108195f0aeadbe8a2bc))

## [1.14.1](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.14.0...v1.14.1) (2023-07-11)


### Bug Fixes

* don't persist default vars ([b944356](https://gitlab.com/hove-ict/tools/ci/templates/commit/b94435641dbd8c2e817cbe7216f3909273a2682e))

# [1.14.0](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.13.3...v1.14.0) (2023-07-11)


### Bug Fixes

* add mariadb backup ([4d5e310](https://gitlab.com/hove-ict/tools/ci/templates/commit/4d5e31017769bbae6c69be45e68afb765780ab1a))


### Features

* mariadb specific backup ([fc07193](https://gitlab.com/hove-ict/tools/ci/templates/commit/fc071933e579e3bf0e702b8a2eb80bb139713110))

## [1.13.3](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.13.2...v1.13.3) (2023-02-28)


### Bug Fixes

* better default var handling ([d4c642b](https://gitlab.com/hove-ict/tools/ci/templates/commit/d4c642be5b647dafe427de53af8623f620854ec3))

## [1.13.2](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.13.1...v1.13.2) (2023-02-18)


### Bug Fixes

* singular and reference everywhere ([85c92b0](https://gitlab.com/hove-ict/tools/ci/templates/commit/85c92b0dc7dafa7772d652654b9009db27d7580b))

## [1.13.1](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.13.0...v1.13.1) (2023-02-18)


### Bug Fixes

* more specific filter ([bfca67d](https://gitlab.com/hove-ict/tools/ci/templates/commit/bfca67d3bc061655689d14d6d3e4fb6cde284d02))

# [1.13.0](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.12.0...v1.13.0) (2023-02-17)


### Features

* stack deploy override + external compose ([d4d0b3e](https://gitlab.com/hove-ict/tools/ci/templates/commit/d4d0b3ebc36df2c2e1ba9aa069d5fc4db16d2b28))

# [1.12.0](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.11.0...v1.12.0) (2023-02-15)


### Features

* multiple data sources ([3ca9c37](https://gitlab.com/hove-ict/tools/ci/templates/commit/3ca9c370a9fadfbbd6630e58ef18c57fe87668e0))

# [1.11.0](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.10.6...v1.11.0) (2023-02-05)


### Features

* direct backup ([3782a28](https://gitlab.com/hove-ict/tools/ci/templates/commit/3782a28ddefe065d8cf3f720329abd0f2ae2da64))

## [1.10.6](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.10.5...v1.10.6) (2023-02-05)


### Bug Fixes

* commit merge problems ([4b14f21](https://gitlab.com/hove-ict/tools/ci/templates/commit/4b14f21b48c1233684366ed8623ca8cabf62ab56))

## [1.10.5](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.10.4...v1.10.5) (2023-02-04)


### Bug Fixes

* revert: network stay in build stage ([93fc4a6](https://gitlab.com/hove-ict/tools/ci/templates/commit/93fc4a6d959b870307b37a74cdad37452982593b))

## [1.10.4](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.10.3...v1.10.4) (2023-02-04)


### Bug Fixes

* network setup in .pre ([74e5942](https://gitlab.com/hove-ict/tools/ci/templates/commit/74e5942c552bd66099433ff3d81dbc13b120af2c))

## [1.10.3](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.10.2...v1.10.3) (2023-01-29)


### Bug Fixes

* use compose config for data ([8c5e960](https://gitlab.com/hove-ict/tools/ci/templates/commit/8c5e960adc004946b34ff3992be300536b3b2768))

## [1.10.2](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.10.1...v1.10.2) (2023-01-28)


### Bug Fixes

* better location for this template ([689e0bf](https://gitlab.com/hove-ict/tools/ci/templates/commit/689e0bfb13e1d74bdb237604c508e1c55dfed173))

## [1.10.1](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.10.0...v1.10.1) (2023-01-28)


### Bug Fixes

* use non-overlapping var name ([0a962ad](https://gitlab.com/hove-ict/tools/ci/templates/commit/0a962adc99de87a56babdeabb95b5b4d9e41a585))

# [1.10.0](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.9.2...v1.10.0) (2023-01-28)


### Features

* automatically determine networks ([6327577](https://gitlab.com/hove-ict/tools/ci/templates/commit/6327577418359022de6dcb22a73ef2e8e0e50bd5))

## [1.9.2](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.9.1...v1.9.2) (2023-01-21)


### Bug Fixes

* push ([ec53fa0](https://gitlab.com/hove-ict/tools/ci/templates/commit/ec53fa0a410b7dd36e4a43373c84136fb638c22b))

## [1.9.1](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.9.0...v1.9.1) (2023-01-20)


### Bug Fixes

* actually update service ([4ad471c](https://gitlab.com/hove-ict/tools/ci/templates/commit/4ad471c71f7877e9d01ad8760065736594ea229a))

# [1.9.0](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.8.0...v1.9.0) (2023-01-15)


### Features

* sqlite backup ([0895bca](https://gitlab.com/hove-ict/tools/ci/templates/commit/0895bca51e38d93079625c8b3c8035fde0ee4608))

# [1.8.0](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.7.3...v1.8.0) (2023-01-15)


### Features

* compose deploy ([6c0ce7f](https://gitlab.com/hove-ict/tools/ci/templates/commit/6c0ce7f01f6b8115d8dca26dec8ca47442088c0f))

## [1.7.3](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.7.2...v1.7.3) (2023-01-15)


### Bug Fixes

* use non-overlapping job name ([4277454](https://gitlab.com/hove-ict/tools/ci/templates/commit/4277454ed6b9ee28a3f0df4a07006ccf0ee04f41))

## [1.7.2](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.7.1...v1.7.2) (2023-01-15)


### Bug Fixes

* network doesnt need previous jobs ([b985866](https://gitlab.com/hove-ict/tools/ci/templates/commit/b9858669749f585f80404035c84be3247ae73bb0))

## [1.7.1](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.7.0...v1.7.1) (2023-01-15)


### Bug Fixes

* use yq for reliable image and service name ([360fb97](https://gitlab.com/hove-ict/tools/ci/templates/commit/360fb9704538f862beb201576b36000db017a8f0))

# [1.7.0](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.6.1...v1.7.0) (2023-01-15)


### Features

* deploy password ([711f09c](https://gitlab.com/hove-ict/tools/ci/templates/commit/711f09c69a72c01dde38566b590047e3fcfac3a9))

## [1.6.1](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.6.0...v1.6.1) (2023-01-15)


### Bug Fixes

* allow for network pre-/suffix ([c40d585](https://gitlab.com/hove-ict/tools/ci/templates/commit/c40d5855800f47e2cc1cf2c8565701713b549c51))

# [1.6.0](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.5.2...v1.6.0) (2023-01-15)


### Features

* update stack containers ([d86623f](https://gitlab.com/hove-ict/tools/ci/templates/commit/d86623f0cc0bf0c001ff26eec95d13197ff42aef))

## [1.5.2](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.5.1...v1.5.2) (2023-01-15)


### Bug Fixes

* better separation ([89dcfe8](https://gitlab.com/hove-ict/tools/ci/templates/commit/89dcfe8f3bfacd0716220bac3f41454c18c68c08))

## [1.5.1](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.5.0...v1.5.1) (2023-01-15)


### Bug Fixes

* ci paths ([c0673d3](https://gitlab.com/hove-ict/tools/ci/templates/commit/c0673d385008177ccb6f19a7b9a75768ee9266f6))

# [1.5.0](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.4.4...v1.5.0) (2023-01-15)


### Features

* stack deploy ([1eb6502](https://gitlab.com/hove-ict/tools/ci/templates/commit/1eb65025506fa3943a5a00964b5970d812cf2fdd))

## [1.4.4](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.4.3...v1.4.4) (2023-01-15)


### Bug Fixes

* move backup to separate dir ([97945e4](https://gitlab.com/hove-ict/tools/ci/templates/commit/97945e4639ce86e9d50f10ffadfc600004419dcc))

## [1.4.3](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.4.2...v1.4.3) (2023-01-11)


### Bug Fixes

* verbose mkdirs ([b565a45](https://gitlab.com/hove-ict/tools/ci/templates/commit/b565a45deefd10c925d787df47630a36bd9d72c2))

## [1.4.2](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.4.1...v1.4.2) (2023-01-11)


### Bug Fixes

* more predictable service name ([38a0928](https://gitlab.com/hove-ict/tools/ci/templates/commit/38a0928be19032b1601841eda278b37fb869988f))

## [1.4.1](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.4.0...v1.4.1) (2023-01-10)


### Bug Fixes

* copy folder content ([145bd3d](https://gitlab.com/hove-ict/tools/ci/templates/commit/145bd3db04f79775141bbd2bda6eaf8149724bfa))

# [1.4.0](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.3.0...v1.4.0) (2023-01-10)


### Features

* better allow multiple jobs ([165e229](https://gitlab.com/hove-ict/tools/ci/templates/commit/165e22943e7f879dfe9c6979b8fb5d0f6757ffc6))

# [1.3.0](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.2.0...v1.3.0) (2023-01-03)


### Features

* generalize data backup script ([9d6107d](https://gitlab.com/hove-ict/tools/ci/templates/commit/9d6107d02881177bb820125e0c3ca7b8d80b1737))

# [1.2.0](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.1.0...v1.2.0) (2023-01-03)


### Features

* generalize db backup scripts ([7dfc187](https://gitlab.com/hove-ict/tools/ci/templates/commit/7dfc18724bf3380bce2e79a4f54a9a7baf510217))

# [1.1.0](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.0.1...v1.1.0) (2022-12-25)


### Features

* allow setting container name ([79bd7ad](https://gitlab.com/hove-ict/tools/ci/templates/commit/79bd7ad4cf99d7ff0180576702d2db440a2ad651))

## [1.0.1](https://gitlab.com/hove-ict/tools/ci/templates/compare/v1.0.0...v1.0.1) (2022-12-12)


### Bug Fixes

* add base script to db default; reference script ([d821ef0](https://gitlab.com/hove-ict/tools/ci/templates/commit/d821ef079423aefabc5532a5b3f382cc78250e53))

# 1.0.0 (2022-12-12)


### Bug Fixes

* ensure full initial repo state ([f045d7d](https://gitlab.com/hove-ict/tools/ci/templates/commit/f045d7d484fcf5ee0a9571c456434a666d61e48d))
* run on any protected branch ([eb60615](https://gitlab.com/hove-ict/tools/ci/templates/commit/eb606155aaa555bbf54536f20ded5c4245faecf8))
* separate commands ([db44ea6](https://gitlab.com/hove-ict/tools/ci/templates/commit/db44ea657c1f945be59cf3a964b66a58c63c21ab))


### Features

* compress files ([325d4e3](https://gitlab.com/hove-ict/tools/ci/templates/commit/325d4e39574df5899c46c61f23e36319c3c3d906))
