## Backups

### Examples

#### Backup Postgres database and data container

```yaml
---
include:
  - project: hove-ict/tools/ci/templates
    file: backup/backup.yml
    ref: v1.18.1

variables:
  PROJECT_NAME: my-app

backup:
  script:
    - SERVICE_NAME=my-app
    - BACKUP_SOURCE=/persistent_data
    - BACKUP_TARGET=data/my-app # Always relative to $BACKUP_ROOT, which defaults to ${CI_PROJECT_DIR}/backup/
    - !reference [".backup:data", script]
    - SERVICE_NAME=mariadb
    - !reference [".backup:database:postgres", script]
    - !reference [".backup:commit", script]
```

#### Backup multiple data containers

```yaml
---
include:
  - project: hove-ict/tools/ci/templates
    file: backup/backup.yml
    ref: v1.18.1

variables:
  PROJECT_NAME: my-app

backup:
  script:
    - SERVICE_NAME=homeassistant-1
    - BACKUP_SOURCE=/config
    - BACKUP_TARGET=data/home-assistant
    - !reference [".backup:data", script]
    - SERVICE_NAME=zigbee2mqtt-1
    - BACKUP_SOURCE=/app/data
    - BACKUP_TARGET=data/zigbee2mqtt
    - !reference [".backup:data", script]
    - SERVICE_NAME=frigate-1
    - BACKUP_SOURCE=/config
    - BACKUP_TARGET=data/frigate
    - !reference [".backup:data", script]
    - !reference [".backup:commit", script]
```

### Restore

#### MySQL / MariaDB

```shell
docker exec -i ${CONTAINER} mysql --user ${MYSQL_USER} -p${MYSQL_PASSWORD} < database.sql
```
